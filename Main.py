from Game import *

repeat = True

def creating_board(game):
    repeat = True
    while repeat:
        row = int(input("Please insert row size: "))
        col = int(input("Please insert column size:" ))
        if row < 3 or col < 3:
            print("These size is invalid ... \n")
        else:
            game.create_board(row, col)
            repeat = False

def player_turn(game):
    valid = False
    while not valid:
        i, j = input("Select cell you want to insert with (x, y): ").split()
        ans = input("Select the char you want to insert with (S or O): ")
        row, col = int(i) - 1, int(j) - 1
        if not ((0 <= row < game.board_row) or (0 <= col < game.board_col)
           or (ans.lower() == 's') or (ans.lower() == 'o')):
            print("Input is invalid ...\n")
        else:
            print("You put", ans.capitalize(), "on cell", row, col,"!")
            ans = True if ans.lower() == 's' else False
            game.fill_board(ans, row, col)


while repeat:
    print("Welcome in SOS Board Game!")
    game = Game()
    creating_board(game)
    while not game.is_finished:
        player_turn(game)
    print("Game Over ....")
    print("Want to play again?")
    answer = input("Yes for play again, No for not \n")
    if answer.lower() == "no":
        repeat = False
        print("Thank you for playing ... !")

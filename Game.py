class Game:
    """
    docstring
    """
    def __init__(self):
        self.player_score = 0
        self.opponent_score = 0
        self.to_be_checked_by_opponent = []
        self.board = [[None]]
        self.is_player_turn = True
        self.is_finished = False
        self.S = True #S in board
        self.O = False #O in board
        self.X = None #Blank board

    def access_cell(self, row, col, data):
        return data[row][col]

    def duplicate_list(self, data):
        if type(data[0]) == type([]):
            return [i.copy() for i in data]
        else:
            return data.copy()

    def edit_board(self, data, input, row, col):
        data[row][col] = input

    def print_board(self):
        for row in self.board:
            s = '['
            for cell in row:
                if cell == self.S:
                    s += '"S" '
                elif cell == self.O:
                    s += '"O" '
                else:
                    s+= '"X" '
            s += ']'
            print(s)

    def print_score(self):
        print("Player's score:", self.player_score)
        print("Opponent's score:", self.opponent_score)
    
    def print_turn(self):
        if self.is_player_turn:
            print("Now is player turn ...")
        else:
            print("Now is opponent turn")

    def print_status(self):
        self.print_board()
        self.print_score()
        self.print_turn()
        if self.is_finished:
            print("Game is finished ...")
            if self.player_score > self.opponent_score:
                print("Player wins!")
            elif self.player_score < self.opponent_score:
                print("Opponent wins!")
            else:
                print("The game is draw!")

    def generate_neighbors(self, x, y):
        return [(i,j) for i in range(x-1, x+2) for j in range(y-1,y+2) 
        if ((0 <= i < self.board_row) and (0 <= j < self.board_col) and not ((i == x) and (j == y)))]
    
    def create_board(self, row, col):
        board = []
        for i in range(row):
            proceeded_row = []
            for j in range(col):
                proceeded_row.append(self.X)
            board.append(proceeded_row)
        self.board = board
        self.board_row = row
        self.board_col = col
        self.print_status()

    def remove_checked_by_opponent(self, data, x, y):
        input = (x, y)
        if input in data:
            data.remove(input)
    
    def evaluate_score_and_turn(self, get_score):
        if get_score > 0:
            if self.is_player_turn:
                self.player_score += get_score
                print("Player gets", get_score,"point(s)!")
            else:
                self.opponent_score += get_score
                print("Opponent gets", get_score,"point(s)!")
        else:
            self.is_player_turn = not self.is_player_turn

    def evaluate_is_finished(self):
        halted = False
        for i in range(self.board_row):
            if halted:
                break
            for j in range(self.board_col):
                if self.access_cell(i, j, self.board) == self.X:
                    halted = True
                    break
                
        if not halted:
            self.is_finished = True

    def fill_board(self, input, x, y):
        self.edit_board(self.board, input, x, y)
        self.remove_checked_by_opponent(self.to_be_checked_by_opponent, x, y)
        self.update_to_be_checked_by_opponent(self.to_be_checked_by_opponent, x, y, self.board)
        get_score = self.evaluate_is_SOS(input, x, y, self.board)
        self.evaluate_score_and_turn(get_score)
        self.evaluate_is_finished()
        self.print_status()
        if not (self.is_player_turn or self.is_finished):
            self.opponent_turns()

    def update_to_be_checked_by_opponent(self, data, x, y, board):
        neighbors = self.generate_neighbors(x, y)
        for element in neighbors:
            i, j = element
            if self.access_cell(i, j, board) != self.X:
                neighbors.remove(element)

        for element in data:
            if element in neighbors:
                neighbors.remove(element)
                continue
        
        for element in neighbors:
            data.append(element)

        """
        same_element = [(neighbor) for neighbor in neighbors for element in self.to_be_checked_by_opponent
        if element == neighbor]

        for element in same_element:
            neighbors.remove(element)

        for element in neighbors:
            self.to_be_checked_by_opponent.append(element)
        """

    def evaluate_is_SOS(self, input, x, y, data):
        neighbors = self.generate_neighbors(x, y)
        for element in range(len(neighbors)-1, 0, -1):
            i, j = neighbors[element]
            if self.access_cell(i, j, data) == self.X:
                neighbors.remove(neighbors[element])
        
        if len(neighbors) == 0:
            return 0
        else:
            sos_count = 0
            for element in neighbors:
                i, j = element
                opposite_i = i+(i-x) if input == self.S else x+(x-i)
                opposite_j = j+(j-y) if input == self.S else y+(y-j)
                if not ((0 <= opposite_i < self.board_row) and (0 <= opposite_j < self.board_col)):
                    continue
                elif ((input == self.S == self.access_cell(opposite_i, opposite_j, data)) and (self.access_cell(i, j, data) == self.O)):
                    sos_count += 1
                elif ((self.access_cell(i, j, data) == self.access_cell(opposite_i, opposite_j, data) == self.S) and (input == self.O)):
                    sos_count += 1
            return sos_count if input == self.S else sos_count//2 #SOS will count twice if O insertion (simmetry property)

    def opponent_thinking(self, options, board, heuristic, is_opponent_turn, repeat):
        heuristics = []
        for element in options:
            x, y = element
            heuristics.append(self.evaluate_is_SOS(self.S, x, y, board))
            heuristics.append(self.evaluate_is_SOS(self.O, x, y, board))
        
        results = []
        for i in range(len(options)):
            if repeat:
                x, y = options[i]
                new_board = self.duplicate_list(board)
                self.edit_board(new_board, self.S, x, y)
                new_options = self.duplicate_list(options)
                self.remove_checked_by_opponent(new_options, x, y)
                self.update_to_be_checked_by_opponent(new_options, x, y, new_board)
                                
                new_opponent_turn = not is_opponent_turn if heuristics[i*2] == 0 else is_opponent_turn
                results.append(self.opponent_thinking(new_options, new_board, heuristics[i*2], new_opponent_turn, False))

                self.edit_board(new_board, self.O, x, y)
                new_opponent_turn = not is_opponent_turn if heuristics[i*2+1] == 0 else is_opponent_turn
                results.append(self.opponent_thinking(new_options, new_board, heuristics[i*2+1], new_opponent_turn, False))
            else:
                temp1 = None
                temp2 = None
                if is_opponent_turn:
                    temp1 = heuristic + heuristics[i*2]
                    temp2 = heuristic + heuristics[i*2+1]
                else:
                    temp1 = heuristic - heuristics[i*2]
                    temp2 = heuristic - heuristics[i*2+1]
                results.append((self.S, options[i], temp1))
                results.append((self.O, options[i], temp2))
        
        min_val = None
        min_index = None
        max_val = None
        max_index = None
        for i in range(len(results)):
            input, element, val = results[i]
            if i == 0:
                min_val = val
                max_val = val
                min_index = i
                max_index = i
            elif val > max_val:
                max_val = val
                max_index = i
            elif val < min_val:
                min_val = val
                min_index = i
        return results[max_index] if is_opponent_turn else results[min_index]

    def opponent_turns(self):
        if not self.is_player_turn:
            option_to_check = self.duplicate_list(self.to_be_checked_by_opponent)
            duplicate_board = self.duplicate_list(self.board)
            chosen = self.opponent_thinking(option_to_check, duplicate_board, 0, True, True)
            input, index, val = chosen
            i, j = index
            self.fill_board(input, i, j)

